//
//  CionAsset.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import Foundation

struct CoinAsset: Codable {
    var id: String = ""
    var rank: String = ""
    var symbol: String = ""
    var name: String = ""
    var supply: String = ""
    var maxSupply: String? = ""
    var marketCapUsd: String? = ""
    var priceUsd: String? = ""
}
