//
//  AssetService.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import Foundation

class AssetService: NSObject {

    private let api = ApiManager.shared
    
    func fetchAssets(success: @escaping ([CoinAsset]) -> (),
                     fail: @escaping (String) -> ()) {
        api.request(path: .getAssets, body: nil, header: nil, method: .get) { (response) -> (Void) in
            if let result = response["data"] as? [[String: Any]] {
                let jsonData = try? JSONSerialization.data(withJSONObject: result)
                let assets = try! JSONDecoder().decode([CoinAsset].self, from: jsonData!)
                success(assets)
            }
        } onFailure: { (error) in
            fail(error)
        }
    }
}
