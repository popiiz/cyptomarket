//
//  AssetsViewModel.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import Foundation
import UIKit
import Kingfisher

class AssetsViewModel: ManInteractor {
    
    var assets: [CoinAsset] = []
    var assetsTemp: [CoinAsset] = []
    
    private var service: AssetService!
    
    // MARK: - Initializers
    init(service: AssetService) {
        self.service = service
    }
    
    func apiFetchAssets() {
        service.fetchAssets { (assets) in
            self.assets = assets
            self.assetsTemp = assets
            self.onSuccessCallback?(true)
        } fail: { (error) in
            self.onErrorCallback?(error)
        }
    }
    
    func displayAssets(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CoinAssetTableViewCell", for: indexPath) as? CoinAssetTableViewCell {
            let asset = assets[indexPath.row]
            cell.setupInterface(coinAsset: asset)
            return cell
        }
        return UITableViewCell()
    }
    
    func searchCoin(text: String) {
        assets = assetsTemp.filter { (asset: CoinAsset) -> Bool in
            return asset.name.lowercased().contains(text.lowercased()) ||
                asset.symbol.lowercased().contains(text.lowercased())
        }
        
        if text.isEmpty {
            assets = assetsTemp
        }
        self.onSuccessCallback?(true)
    }
}
