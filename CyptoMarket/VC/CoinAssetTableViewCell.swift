//
//  CoinAssetTableViewCell.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import UIKit

class CoinAssetTableViewCell: UITableViewCell {

    @IBOutlet weak var tokenNameLabel: UILabel!
    @IBOutlet weak var tokenPriceLabel: UILabel!
    @IBOutlet weak var tokenMaketCapLabel: UILabel!
    @IBOutlet weak var otherLabel: UILabel!
    @IBOutlet weak var tokenImageView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //tokenImageView.layer.masksToBounds = true
        tokenImageView.layer.cornerRadius = tokenImageView.frame.height / 2
        cardView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupInterface(coinAsset: CoinAsset) {
        tokenNameLabel.text = coinAsset.symbol
        tokenMaketCapLabel.text = coinAsset.name
        tokenPriceLabel.text = covertPrice(str: coinAsset.priceUsd ?? "0.0")
        
        if let url = URL(string: "https://assets.coincap.io/assets/icons/\(coinAsset.symbol.lowercased())@2x.png") {
            tokenImageView.kf.setImage(with: url)
        }
    }
    
    func covertPrice(str: String) -> String {
        let price = Double(str) ?? 0.0
        return String(format: "$%.03f", price)
    }

}
