//
//  ManInteractor.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import Foundation

class ManInteractor: NSObject {
    
    var onErrorCallback: ((String) -> Void)?
    var onSuccessCallback: ((Bool) -> Void)?
}
