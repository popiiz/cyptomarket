//
//  ApiManager.swift
//  CyptoMarket
//
//  Created by Saowalak Rungrat on 12/5/2564 BE.
//

import Alamofire
import SystemConfiguration
import UIKit

class ApiManager: NSObject {
    static let shared = ApiManager()
    private var manager: Alamofire.Session?
    private var baseUrl = "https://api.coincap.io/v2/"
    
    func getManager() -> Alamofire.Session {
        if let m = self.manager {
            return m
        } else {
            let defaultManager: Alamofire.Session = {
                let serverTrustManager = ServerTrustManager(evaluators: [
                    "147.50.22.61:8080": DisabledTrustEvaluator(),
                ])
                
                let session = Session(serverTrustManager: serverTrustManager)

                let configuration = URLSessionConfiguration.default
                configuration.headers = .default
                configuration.timeoutIntervalForRequest = 60
                
                let manager = Session.init(configuration: configuration,
                                           delegate: session.delegate,
                                           startRequestsImmediately: true,
                                           cachedResponseHandler: nil)
                return manager
            } ()
            
            self.manager = defaultManager
            return self.manager!
        }
    }

    func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }
    
    func request(path: ApiConstant,
                 body: [String:Any]?,
                 header: [String: Any]?,
                 param: String = "",
                 method: HTTPMethod = .post,
                 onSuccess: @escaping ([String: Any]) -> (Void),
                 onFailure: @escaping ((String) -> Void)) {
        if connectedToNetwork() {
            var urlString = "\(baseUrl)\(path.rawValue)"
            if param != "" {
                urlString = "\(urlString)?\(param)"
            }
            
            if method == .get {
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
            
            if let url = URL(string: urlString) {
                var request = URLRequest(url: url)
                request.method = method
                request.headers = self.getCommonHeader(headers: header)
                
                print("=============== Start: Call Service ===============")
                print("URL :", url)
                print("Method : \(method.rawValue)")
                
                if body != nil{
                    request.httpBody = try! JSONSerialization.data(withJSONObject: body!)
                    print("********** body **********")
                    print(body as Any)
                }
                
                AF.request(request).validate(statusCode: 200...500).responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        print("********** response **********")
                        if response.response!.statusCode < 300 {
                            if let value = response.value as? [String:Any] {
                                if let data = value["data"] as? [String:Any] {
                                    onSuccess(data)
                                } else if let data = value["data"] as? [[String:Any]] {
                                    onSuccess(["data": data])
                                } else{
                                    onSuccess([String:Any]())
                                }
                            }
                        } else if response.response!.statusCode == 401 {
                            //
                            break
                        } else {
                            onFailure("Something wrong")
                        }
                        break
                    case .failure(let error):
                        onFailure(error.localizedDescription)
                        break
                    }
                    print("=============== End: Call Service ===============")
                })
            } else {
                onFailure("Please connect to internet while using the application")
            }
        } else {
            onFailure("Please connect to internet while using the application")
        }
    }
    
    func getCommonHeader(headers: [String: Any]?) -> HTTPHeaders {
        var commonParam: HTTPHeaders = [
            "Content-Type": "application/json",
        ]
        
        if headers != nil {
            headers!.forEach({ (key, value) in
                commonParam.add(name: key, value: "\(value)")
            })
        }
        return commonParam
    }
}
